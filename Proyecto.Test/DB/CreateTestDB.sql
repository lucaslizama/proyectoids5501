drop database ids5501_test;
create database ids5501_test;
use ids5501_test;

create table tecnico
(
    id int auto_increment,
    nombre varchar(50) not null,
    apellido varchar(50) not null,
    username varchar(50) not null,
    password varchar(200) not null,
    primary key(id),
    unique (username)
);

create table pacientes
(
    id int auto_increment,
    rut int unique,
    dv varchar(1) not null,
    nombre varchar(50) not null,
    apellido_pat varchar(50) not null,
    apellido_mat varchar(50) not null,
    hospitalizado boolean not null,
    primary key(id)
);
