using NUnit.Framework;
using System;
using Proyecto.Biblioteca.Actores;

namespace Proyecto.Test {

    public class PacienteDAOTest {
        PacienteDAO dao = new PacienteDAO();

        [Test]
        public async void RecuperarUnPacientePorSuRut() {
            if (!await dao.ExistePaciente(18464695)) {
                await dao.Insertar(new Paciente {
                    Rut = 18464695,
                    Dv = '1',
                    Nombre = "Lucas",
                    ApellidoPat = "Lizama",
                    ApellidoMat = "Monje",
                    Hospitalizado = true
                });
            }
            Assert.IsNotNull(await dao.RecuperarPorRut(18464695));
        }

        [Test]
        public async void RecuperarPacienteInexistente() {
            if (await dao.ExistePaciente(12345678)) {
                await dao.Eliminar(12345678);
            }
            Assert.IsNull(await dao.RecuperarPorRut(12345678));
        }

        [Test]
        public async void InsertarNuevoPaciente() {
            if (await dao.ExistePaciente(12345678)) {
                await dao.Eliminar(12345678);
            }
            Paciente paciente = new Paciente {
                Rut = 12345678, Dv = 'K', Nombre = "Jorge",
                ApellidoPat = "Lizama", ApellidoMat = "Monje",
                Hospitalizado = true
            };
            Assert.IsTrue(await dao.Insertar(paciente));
        }

        [Test]
        public async void InsertarPacienteYaExistente() {
            Paciente paciente = new Paciente {
                Rut = 12345678, Dv = 'K', Nombre = "Jorge",
                ApellidoPat = "Lizama", ApellidoMat = "Monje",
                Hospitalizado = true
            };
            if (!await dao.ExistePaciente(12345678)) {
                await dao.Insertar(paciente);
            }
            Assert.IsFalse(await dao.Insertar(paciente));
        }

        [Test]
        public void DarDeAltaUnPaciente() {
            throw new NotImplementedException();
        }

        [Test]
        public void HospitalizarAUnPaciente() {
            throw new NotImplementedException();
        }

        [Test]
        public void DarDeAltaUnPacienteNoHospitalizado() {
            throw new NotImplementedException();
        }

        [Test]
        public void HospitalizarPacienteHospitalizado() {
            throw new NotImplementedException();
        }

        [Test]
        public void ActualizarDatosPaciente() {
            throw new NotImplementedException();
        }

        [Test]
        public void ActualizarRutPaciente() {
            throw new NotImplementedException();
        }
    }
}