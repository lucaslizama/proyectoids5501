using NUnit.Framework;
using Proyecto.Biblioteca.Actores;
using System;

namespace Proyecto.Test {

    public class TecnicoDAOTest {

        [Test]
        public async void InsertarTecnico() {
            TecnicoDAO dao = new TecnicoDAO();
            Tecnico t = new Tecnico() {
                Nombre = "Gabriel",
                Apellido = "Torres",
                Username = "gtorres",
                Password = "password123"
            };
            bool exito = await dao.Insertar(t);
            Assert.IsTrue(exito);
            Assert.IsTrue(await dao.ExisteTecnico("gtorres"));
            await dao.Eliminar(t.Username);
        }

        [Test]
        public async void RecuperarTecnicoPorUsername() {
            TecnicoDAO dao = new TecnicoDAO();
            if (!await dao.ExisteTecnico("madsushi"))
                await dao.Insertar(new Tecnico {
                    Nombre = "Ian",
                    Apellido = "Genkowski",
                    Username = "madsushi",
                    Password = "lol1234"
                });
            Tecnico t = await dao.RecuperarPorUsername("madsushi");
            Assert.IsNotNull(t, "No se encontro al tecnico \"madsushi\"");
        }

        [Test]
        public async void ComprobarExistenciaTecnico() {
            TecnicoDAO dao = new TecnicoDAO();
            bool existe = await dao.ExisteTecnico("dene092");
            Assert.IsTrue(existe);
        }

        [Test]
        public async void EliminarTecnico() {
            TecnicoDAO dao = new TecnicoDAO();
            if (!await dao.ExisteTecnico("madsushi")) {
                await dao.Insertar(new Tecnico {
                    Nombre = "Ian",
                    Apellido = "Genkowski",
                    Username = "madsushi",
                    Password = "lol1234"
                });
            }
            Tecnico respaldo = await dao.RecuperarPorUsername("madsushi");
            Assert.IsNotNull(respaldo, "No se encontro al tecnico \"madsushi\"");
            bool exito = await dao.Eliminar("madsushi");
            Assert.IsTrue(exito, "No se pudo eliminar al tecnico \"madsushi\"");
            await dao.Insertar(respaldo);
        }

        [Test]
        [Ignore("Sera implementado pronto")]
        public void ActualizarTecnico() {
            throw new NotImplementedException();
        }

        [Test]
        public async void AutorizarLoginExitoso() {
            TecnicoDAO dao = new TecnicoDAO();
            string username = "dene092";
            string password = "8330719";
            Assert.IsTrue(await dao.ExisteTecnico(username),"No existe la tecnico dene092 en la base de datos.");
            Assert.IsTrue(await dao.Autorizar(username,password),"Contraseņa incorrecta");
        }

        [Test]
        public async void AutorizarLoginPasswordIncorrecta() {
            TecnicoDAO dao = new TecnicoDAO();
            string username = "dene092";
            string password = "triforce#";

            Assert.IsTrue(await dao.ExisteTecnico(username), "No existe la tecnico dene092 en la base de datos.");
            Assert.IsFalse(await dao.Autorizar(username,password));
        }
    }
}