using NUnit.Framework;
using Proyecto.Biblioteca.Actores;
using System;
using System.Collections.Generic;
using System.Json;

namespace Proyecto.Test {

    public class PacienteFactoryTest {

        [Test]
        public void CrearPacienteDesdeJson() {
            Dictionary<string, JsonValue> datos = new Dictionary<string, JsonValue>();
            datos.Add("rut",18464695);
            datos.Add("dv",'1');
            datos.Add("nombre", "Lucas");
            datos.Add("apellido_pat", "Lizama");
            datos.Add("apellido_mat", "Monje");
            datos.Add("hospitalizado", true);
            JsonValue json = new JsonObject(datos) as JsonValue;

            Paciente paciente = PacienteFactory.CrearPaciente(json);
            Assert.IsNotNull(paciente);
            Assert.AreEqual(paciente.Rut, (int)json["rut"]);
            Assert.AreEqual(paciente.Dv, (char)json["dv"]);
            Assert.AreEqual(paciente.Nombre, (string)json["nombre"]);
            Assert.AreEqual(paciente.ApellidoPat, (string)datos["apellido_pat"]);
            Assert.AreEqual(paciente.ApellidoMat, (string)datos["apellido_mat"]);
            Assert.IsTrue(paciente.Hospitalizado);
        }
    }
}