using NUnit.Framework;
using Proyecto.Biblioteca.Actores;
using System.Collections.Generic;
using System.Json;

namespace Proyecto.Test {

    public class TecnicoFactoryTest {

        [Test]
        public void CrearTecnicoDesdeUnObjectoJson() {
            Dictionary<string, JsonValue> datos = new Dictionary<string, JsonValue>();
            datos.Add("nombre", "Lucas");
            datos.Add("apellido", "Lizama");
            datos.Add("username", "lucaslizama");
            datos.Add("password", "triforce#");
            JsonValue json = new JsonObject(datos) as JsonValue;

            Tecnico tecnico = TecnicoFactory.CrearTecnico(json);
            Assert.IsNotNull(tecnico);
            Assert.AreEqual(tecnico.Nombre, (string)json["nombre"]);
            Assert.AreEqual(tecnico.Apellido, (string)datos["apellido"]);
            Assert.AreEqual(tecnico.Username, (string)datos["username"]);
            Assert.AreEqual(tecnico.Password, (string)datos["password"]);
        }
    }
}