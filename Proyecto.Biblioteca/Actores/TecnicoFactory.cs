﻿using System.Json;

namespace Proyecto.Biblioteca.Actores {

    public class TecnicoFactory
    {
        public static Tecnico CrearTecnico(JsonValue data)
        {
            Tecnico tecnico = null;
            if(!data.ContainsKey("error")){
                tecnico = new Tecnico();
                tecnico.Nombre = data["nombre"];
                tecnico.Apellido = data["apellido"];
                tecnico.Username = data["username"];
                tecnico.Password = data["password"];
            }
            return tecnico;
        }
    }
}