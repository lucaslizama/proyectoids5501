﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Json;

namespace Proyecto.Biblioteca.Actores {

    public class Tecnico
    {
        private string nombre;
        private string apellido;
        private string username;
        private string password;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public Tecnico()
        {
            nombre = string.Empty;
            apellido = string.Empty;
            username = string.Empty;
            password = string.Empty;
        }

        public Dictionary<string,string> ToDictionary() {
            Dictionary<string,string> datos = new Dictionary<string, string>();
            datos.Add("nombre", nombre);
            datos.Add("apellido", apellido);
            datos.Add("username", username);
            datos.Add("password", password);
            return datos;
        }

        public string ToJson() {
            Dictionary<string,JsonValue> datos = new Dictionary<string, JsonValue>();
            datos.Add("nombre",nombre);
            datos.Add("apellido", apellido);
            datos.Add("username", username);
            datos.Add("password", password);
            JsonObject json = new JsonObject(datos);
            return json.ToString();
        }
    }
}