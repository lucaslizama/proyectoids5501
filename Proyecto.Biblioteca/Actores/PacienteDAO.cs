using System;
using System.IO;
using System.Json;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Proyecto.Biblioteca.Actores {

    public class PacienteDAO {
        private const string host = "http://201.215.42.227:8000";

        public async Task<Paciente> RecuperarPorRut(int rut) {
            string url = host + "/paciente/recuperar";

            using (HttpClient client = new HttpClient()) {
                Dictionary<string, string> values = new Dictionary<string, string>();
                values.Add("rut", rut.ToString());
                FormUrlEncodedContent content = new FormUrlEncodedContent(values);
                HttpResponseMessage response = await client.PostAsync(url, content);
                JsonValue json = JsonObject.Parse(await response.Content.ReadAsStringAsync());
                return PacienteFactory.CrearPaciente(json);
            }
        }

        public async Task<bool> Insertar(Paciente paciente) {
            string url = host + "/paciente/insertar";

            using (HttpClient client = new HttpClient()) {
                FormUrlEncodedContent content = new FormUrlEncodedContent(paciente.ToDictionary());
                HttpResponseMessage response = await client.PostAsync(url, content);
                JsonValue json = JsonObject.Parse(await response.Content.ReadAsStringAsync());
                return json["exito"];
            }
        }

        public async Task<bool> Eliminar(int rut) {
            string url = host + "/paciente/eliminar";

            using (HttpClient client = new HttpClient()) {
                Dictionary<string,string> values = new Dictionary<string, string>();
                values.Add("rut", rut.ToString());
                FormUrlEncodedContent content = new FormUrlEncodedContent(values);
                HttpResponseMessage response = await client.PostAsync(url,content);
                JsonValue json = JsonObject.Parse(await response.Content.ReadAsStringAsync());
                return json["exito"];
            }
        }

        public async Task<bool> DarDeAlta(int rut) {
            throw new NotImplementedException();
        }

        public async Task<bool> Hospitalizar(int rut) {
            throw new NotImplementedException();
        }

        public async Task<bool> Actualizar(Paciente paciente) {
            throw new NotImplementedException();
        }

        public async Task<bool> ExistePaciente(int rut) {
            return await RecuperarPorRut(rut) != null;
        }
    }
}