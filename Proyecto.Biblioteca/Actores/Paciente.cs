using System.Collections.Generic;

namespace Proyecto.Biblioteca.Actores {

    public class Paciente {
        private int rut;
        private char dv;
        private string nombre;
        private string apellidoPat;
        private string apellidoMat;
        private bool hospitalizado;

        public int Rut {
            get { return rut; }

            set { rut = value; }
        }

        public char Dv {
            get { return dv; }

            set { dv = value; }
        }

        public string Nombre {
            get { return nombre; }

            set { nombre = value; }
        }

        public string ApellidoPat {
            get { return apellidoPat; }

            set { apellidoPat = value; }
        }

        public string ApellidoMat {
            get { return apellidoMat; }

            set { apellidoMat = value; }
        }

        public bool Hospitalizado {
            get { return hospitalizado; }

            set { hospitalizado = value; }
        }

        public Paciente() {
            this.rut = 0;
            this.dv = '0';
            this.nombre = string.Empty;
            this.apellidoPat = string.Empty;
            this.apellidoMat = string.Empty;
            this.hospitalizado = false;
        }

        public Dictionary<string, string> ToDictionary() {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("rut", rut.ToString());
            data.Add("dv", dv.ToString());
            data.Add("nombre", nombre);
            data.Add("apellido_pat", apellidoPat);
            data.Add("apellido_mat", apellidoMat);
            data.Add("hospitalizado", hospitalizado ? "1" : "0");
            return data;
        }
    }
}