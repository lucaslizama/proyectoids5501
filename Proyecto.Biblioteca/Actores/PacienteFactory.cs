using System;
using System.Json;

namespace Proyecto.Biblioteca.Actores {

    public class PacienteFactory {

        public static Paciente CrearPaciente(JsonValue data) {
            Paciente paciente = null;
            if (!data.ContainsKey("error")) {
                paciente = new Paciente();
                paciente.Rut = data["rut"];
                paciente.Dv = data["dv"];
                paciente.Nombre = data["nombre"];
                paciente.ApellidoPat = data["apellido_pat"];
                paciente.ApellidoMat = data["apellido_mat"];
                paciente.Hospitalizado = data["hospitalizado"];
            }
            return paciente;
        }
    }
}