﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace Proyecto.Biblioteca.Actores {

    public class TecnicoDAO {
        private const string host = "http://201.215.42.227:8000";

        public TecnicoDAO() {
        }

        public async Task<Tecnico> RecuperarPorUsername(string username) {
            string url = host + "/tecnico/recuperar";

            using (HttpClient client = new HttpClient()) {
                Dictionary<string,string> values = new Dictionary<string, string>();
                values.Add("username",username);

                FormUrlEncodedContent content = new FormUrlEncodedContent(values);
                HttpResponseMessage response = await client.PostAsync(url,content);
                JsonValue json = JsonObject.Parse(await response.Content.ReadAsStringAsync());
                Tecnico tecnico = TecnicoFactory.CrearTecnico(json);
                return tecnico;
            }
        }

        public async Task<bool> Insertar(Tecnico tecnico) {
            string url = host + "/tecnico/insertar";

            using (HttpClient client = new HttpClient()) {
                FormUrlEncodedContent content = new FormUrlEncodedContent(tecnico.ToDictionary());
                HttpResponseMessage response = await client.PostAsync(url,content);
                JsonValue json = JsonObject.Parse(await response.Content.ReadAsStringAsync());
                bool exito = json["exito"];
                return exito;
            }
        }

        public async Task<bool> Actualizar(Tecnico tecnico) {
            throw new NotImplementedException();
        }

        public async Task<bool> Eliminar(string username) {
            string url = host + "/tecnico/eliminar";

            using (HttpClient client = new HttpClient()) {
                Dictionary<string,string> values = new Dictionary<string, string>();
                values.Add("username",username);
                FormUrlEncodedContent content = new FormUrlEncodedContent(values);
                HttpResponseMessage response = await client.PostAsync(url,content);
                JsonValue json = JsonObject.Parse(await response.Content.ReadAsStringAsync());
                bool exito = json["exito"];
                return exito;
            }
        }

        public async Task<bool> EliminarTecnicos(string[] usernames) {
            throw new NotImplementedException();
        }

        public async Task<bool> ExisteTecnico(string username) {
            Tecnico tecnico = await RecuperarPorUsername(username);
            return tecnico != null;
        }

        public async Task<bool> Autorizar(string username,string password) {
            TecnicoDAO dao = new TecnicoDAO();
            string url = host + "/tecnico/autorizar";

            using (HttpClient client = new HttpClient()) {
                Dictionary<string, string> values = new Dictionary<string, string>();
                values.Add("username", username);
                values.Add("password", password);
                FormUrlEncodedContent content = new FormUrlEncodedContent(values);
                HttpResponseMessage response = await client.PostAsync(url,content);
                JsonValue json = JsonObject.Parse(await response.Content.ReadAsStringAsync());
                return json["exito"];
            }
        }
    }
}