using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Json;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Proyecto.Biblioteca.Actores;

namespace Proyecto.GUI {
    [Activity(Label = "MainMenuActivity")]
    public class MainMenuActivity : Activity {
        TextView welcomeLabel;
        Tecnico tecnico;
        TecnicoDAO dao;
        

        protected async override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.MainMenu);

            welcomeLabel = FindViewById<TextView>(Resource.Id.lblBienvenidoTecnico);
            dao = new TecnicoDAO();
            tecnico = TecnicoFactory.CrearTecnico(JsonObject.Parse(Intent.GetStringExtra("tecnico")));
            
            welcomeLabel.Text = "Bienvenido: " + tecnico.Nombre + " " + tecnico.Apellido;
        }
    }
}