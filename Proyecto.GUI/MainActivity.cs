﻿using Android.App;
using Android.Graphics;
using Android.OS;
using Android.Widget;
using Android.Content;
using Proyecto.Biblioteca.Actores;
using System;

namespace Proyecto.GUI {

    [Activity(Label = "Proyecto.GUI", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity {
        private EditText username;
        private EditText password;
        private TextView status;
        private Button login;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Login);

            username = FindViewById<EditText>(Resource.Id.txtUsername);
            password = FindViewById<EditText>(Resource.Id.txtPassword);
            status = FindViewById<TextView>(Resource.Id.lblLoginStatus);
            login = FindViewById<Button>(Resource.Id.btnLogin);
            login.Click += Login_Click;
        }

        private async void Login_Click(object sender, EventArgs e) {
            if (username.Text.Equals(string.Empty) || password.Text.Equals(string.Empty)) {
                CambiarEstado("Estado: Campos Vacios",Color.Red);
                return;
            }

            CambiarEstado("Estado: Cargando...",Color.Orange);

            TecnicoDAO dao = new TecnicoDAO();

            if (!await dao.ExisteTecnico(username.Text)) {
                CambiarEstado("Estado: Tecnico no encontrado",Color.Red);
                return;
            }

            if (await dao.Autorizar(username.Text,password.Text)) {
                CambiarEstado("", Color.LightGreen);
                Intent mainMenu = new Intent(this,typeof(MainMenuActivity));
                Tecnico tecnico = await dao.RecuperarPorUsername(username.Text);
                mainMenu.PutExtra("tecnico",tecnico.ToJson());
                StartActivity(mainMenu);
                return;
            }
            CambiarEstado("Estado: Contraseña incorrecta!",Color.Red);
        }

        private void CambiarEstado(string estado,Color color) {
            status.SetTextColor(color);
            status.Text = estado;
        }


    }
}