﻿using TechTalk.SpecFlow;

namespace Proyecto.Requerimientos.Features_Steps {

    [Binding]
    public class AlterarElTratamientoDeUnPacienteSteps
    {
        [Given(@"que el tecnico ""(.*)"" ""(.*)"" se encuentra registrado en el sistema")]
        public void DadoQueElTecnicoSeEncuentraRegistradoEnElSistema(string p0, string p1)
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"que su username es ""(.*)"" y su password es ""(.*)""")]
        public void DadoQueSuUsernameEsYSuPasswordEs(string p0, string p1)
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"el paciente ""(.*)"" ""(.*)"" con rut (.*) esta hospitalizado")]
        public void DadoElPacienteConRutEstaHospitalizado(string p0, string p1, int p2)
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"que el tecnico ""(.*)"" ""(.*)"" ha ingresado al sistema")]
        public void DadoQueElTecnicoHaIngresadoAlSistema(string p0, string p1)
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"se encuentra en la vista para modificar tratamientos")]
        public void DadoSeEncuentraEnLaVistaParaModificarTratamientos()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"el tecnico seleccione a ""(.*)"" ""(.*)"" de la lista de pacientes")]
        public void CuandoElTecnicoSeleccioneADeLaListaDePacientes(string p0, string p1)
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"el tecnico modifique un tratamiento y precione guardar")]
        public void CuandoElTecnicoModifiqueUnTratamientoYPrecioneGuardar()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"vera cada uno de los medicamentos asignados, sus cantidades y horarios")]
        public void EntoncesVeraCadaUnoDeLosMedicamentosAsignadosSusCantidadesYHorarios()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"podra editarlos o modificarlos")]
        public void EntoncesPodraEditarlosOModificarlos()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"el tratamiento sera guardado")]
        public void EntoncesElTratamientoSeraGuardado()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"el cambio registrado al tecnico ""(.*)"" ""(.*)""")]
        public void EntoncesElCambioRegistradoAlTecnico(string p0, string p1)
        {
            ScenarioContext.Current.Pending();
        }
    }
}