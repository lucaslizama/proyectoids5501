﻿using TechTalk.SpecFlow;

namespace Proyecto.Requerimientos.Features_Steps {

    [Binding]
    public class IngresarUnPacienteAlSistemaSteps
    {
        [Given(@"el paciente ""(.*)"" ""(.*)"" con rut (.*) aun no se encuentra ingresado")]
        public void DadoElPacienteConRutAunNoSeEncuentraIngresado(string p0, string p1, int p2)
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"se encuentra en el formulario de ingreso de pacientes")]
        public void DadoSeEncuentraEnElFormularioDeIngresoDePacientes()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"el tecnico ingrese el rut,nombre y apellido del paciente")]
        public void CuandoElTecnicoIngreseElRutNombreYApellidoDelPaciente()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"precione el boton ingresar")]
        public void CuandoPrecioneElBotonIngresar()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"el paciente sera ingresado al sistema")]
        public void EntoncesElPacienteSeraIngresadoAlSistema()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"esta operacion registrada al tecnico ""(.*)"" ""(.*)""")]
        public void EntoncesEstaOperacionRegistradaAlTecnico(string p0, string p1)
        {
            ScenarioContext.Current.Pending();
        }
    }
}