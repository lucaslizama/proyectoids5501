﻿using TechTalk.SpecFlow;

namespace Proyecto.Requerimientos.Features_Steps {

    [Binding]
    public class AdministrarElTratamientoAUnPacienteSteps
    {
        [Given(@"que el tecnico ""(.*)"" ""(.*)"" se encuentra registrado en el sistema\.")]
        public void DadoQueElTecnicoSeEncuentraRegistradoEnElSistema_(string p0, string p1)
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"que su username es ""(.*)"" y su password es ""(.*)""\.")]
        public void DadoQueSuUsernameEsYSuPasswordEs_(string p0, string p1)
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"el paciente ""(.*)"" ""(.*)"" con rut (.*) tambien ingresado necesita su tratamiento\.")]
        public void DadoElPacienteConRutTambienIngresadoNecesitaSuTratamiento_(string p0, string p1, int p2)
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"que el tecnico Gabriel Torres ha ingresado al sistema")]
        public void DadoQueElTecnicoGabrielTorresHaIngresadoAlSistema()
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"se encuentra en la vista para administrar tratamientos")]
        public void DadoSeEncuentraEnLaVistaParaAdministrarTratamientos()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"el tecnico seleccione a Jorge Lizama de la lista de pacientes")]
        public void CuandoElTecnicoSeleccioneAJorgeLizamaDeLaListaDePacientes()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"el tecnico marque un tratamiento como ""(.*)""")]
        public void CuandoElTecnicoMarqueUnTratamientoComo(string p0)
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"vera los tratamientos y sus horarios")]
        public void EntoncesVeraLosTratamientosYSusHorarios()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"podra marcarlos como ""(.*)""")]
        public void EntoncesPodraMarcarlosComo(string p0)
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"quedara constancia de que Gabriel Torres administro el tratamiento\.")]
        public void EntoncesQuedaraConstanciaDeQueGabrielTorresAdministroElTratamiento_()
        {
            ScenarioContext.Current.Pending();
        }
    }
}