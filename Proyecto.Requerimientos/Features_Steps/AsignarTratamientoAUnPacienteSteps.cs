﻿using TechTalk.SpecFlow;

namespace Proyecto.Requerimientos.Features_Steps {

    [Binding]
    public class AsignarTratamientoAUnPacienteSteps
    {
        [Given(@"que la tecnico ""(.*)"" ""(.*)"" se encuentra registrada en el sistema")]
        public void DadoQueLaTecnicoSeEncuentraRegistradaEnElSistema(string p0, string p1)
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"el paciente ""(.*)"" ""(.*)"" con rut (.*) esta recien hospitalizado")]
        public void DadoElPacienteConRutEstaRecienHospitalizado(string p0, string p1, int p2)
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"que la tecnico ""(.*)"" ""(.*)"" ha ingresado al sistema")]
        public void DadoQueLaTecnicoHaIngresadoAlSistema(string p0, string p1)
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"se encuentra en la vista para asignar tratamientos")]
        public void DadoSeEncuentraEnLaVistaParaAsignarTratamientos()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"la tecnico seleccione a ""(.*)"" ""(.*)"" de la lista de pacientes")]
        public void CuandoLaTecnicoSeleccioneADeLaListaDePacientes(string p0, string p1)
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"el tecnico precione el boton agregar")]
        public void CuandoElTecnicoPrecioneElBotonAgregar()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"Seleccione ""(.*)"" como medicamento")]
        public void CuandoSeleccioneComoMedicamento(string p0)
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"(.*)\.(.*) mg como dosis")]
        public void Cuando_MgComoDosis(int p0, int p1)
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"Como horario Lunes ,Miercoles y Viernes a las (.*):(.*)PM")]
        public void CuandoComoHorarioLunesMiercolesYViernesALasPM(int p0, int p1)
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"precione el boton guardar")]
        public void CuandoPrecioneElBotonGuardar()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"vera una lista vacia de tratamientos")]
        public void EntoncesVeraUnaListaVaciaDeTratamientos()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"podra agregar uno o mas nuevos tratamientos")]
        public void EntoncesPodraAgregarUnoOMasNuevosTratamientos()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"el tratamiento sera asignado al paciente")]
        public void EntoncesElTratamientoSeraAsignadoAlPaciente()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"el cambio registrado a la tecnico ""(.*)"" ""(.*)""")]
        public void EntoncesElCambioRegistradoALaTecnico(string p0, string p1)
        {
            ScenarioContext.Current.Pending();
        }
    }
}