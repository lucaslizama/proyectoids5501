﻿using TechTalk.SpecFlow;

namespace Proyecto.Requerimientos {

    [Binding]
    public class VisualizarElTratamientoDeUnPacienteSteps
    {
        [Given(@"que el tecnico ""(.*)"" ""(.*)"" se encuentra registrada en el sistema")]
        public void DadoQueElTecnicoSeEncuentraRegistradaEnElSistema(string p0, string p1)
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"el paciente ""(.*)"" ""(.*)"" con rut (.*) esta ingresado y hospitalizado")]
        public void DadoElPacienteConRutEstaIngresadoYHospitalizado(string p0, string p1, int p2)
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"tiene asignado Paracetamol de (.*)mg a las (.*)am y (.*) pm todos los dias")]
        public void DadoTieneAsignadoParacetamolDeMgALasAmYPmTodosLosDias(int p0, int p1, int p2)
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"se encuentra en la vista para vizualizar tratamientos")]
        public void DadoSeEncuentraEnLaVistaParaVizualizarTratamientos()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"vera que el paciente tiene asignado Paracetamol de (.*)mg")]
        public void EntoncesVeraQueElPacienteTieneAsignadoParacetamolDeMg(int p0)
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"que debe administrase (.*) veces al dia a las (.*)am y (.*)pm todos los dias")]
        public void EntoncesQueDebeAdministraseVecesAlDiaALasAmYPmTodosLosDias(int p0, int p1, int p2)
        {
            ScenarioContext.Current.Pending();
        }
    }
}