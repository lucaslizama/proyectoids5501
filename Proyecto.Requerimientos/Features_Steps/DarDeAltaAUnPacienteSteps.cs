﻿using TechTalk.SpecFlow;

namespace Proyecto.Requerimientos.Features_Steps {

    [Binding]
    public class DarDeAltaAUnPacienteSteps
    {
        [Given(@"se encuentra en la vista para cambiar el estado de un paciente")]
        public void DadoSeEncuentraEnLaVistaParaCambiarElEstadoDeUnPaciente()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"la tecnico cambie el estado a ""(.*)""")]
        public void CuandoLaTecnicoCambieElEstadoA(string p0)
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"precion el boton guardar")]
        public void CuandoPrecionElBotonGuardar()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"vera, entre otros datos, el estado del paciente como ""(.*)""")]
        public void EntoncesVeraEntreOtrosDatosElEstadoDelPacienteComo(string p0)
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"podra cambiar este estado")]
        public void EntoncesPodraCambiarEsteEstado()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"el paciente sera marcado como ""(.*)""")]
        public void EntoncesElPacienteSeraMarcadoComo(string p0)
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"este cambio quedara registrado a la tecnico ""(.*)"" ""(.*)""")]
        public void EntoncesEsteCambioQuedaraRegistradoALaTecnico(string p0, string p1)
        {
            ScenarioContext.Current.Pending();
        }
    }
}