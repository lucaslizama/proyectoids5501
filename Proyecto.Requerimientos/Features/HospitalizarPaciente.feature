﻿Característica: Hospitalizar a un paciente
	Cuando un paciente es ingresado por primera vez
	es automaticamente marcado como "Hospitalizado".
	Pero para paciente antiguos, ya ingresados, es
	necesario simplemente marcarlos como "Hospitalizados" nuevamente.
	Para esto un tecnico accede al sistema y realiza el cambio.
Antecedentes: 
	Dado que el tecnico "Jose" "Labayru" se encuentra registrada en el sistema
	Y que su username es "joselabayru" y su password es "imthenight"
	Y el paciente "Fernando" "Castillo" con rut 18175963 esta recien hospitalizado

Escenario: Se hospitaliza a un paciente ya existente
	Dado que el tecnico "Jose" "Labayru" ha ingresado al sistema
	Y se encuentra en la vista para cambiar el estado de un paciente
	Cuando el tecnico seleccione a "Fernando" "Castillo" de la lista de pacientes
	Entonces vera, entre otros datos, el estado del paciente como "Dado de Alta"
	Y podra cambiar este estado
	Cuando la tecnico cambie el estado a "Hospitalizado"
	Y precion el boton guardar
	Entonces el paciente sera marcado como "Hospitalizado"
	Y este cambio quedara registrado a el tecnico "Jose" "Labayru"