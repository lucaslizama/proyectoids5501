﻿Característica: Visualizar el tratamiento de un paciente
	En cualquier momento debe ser posible revisar la informacion
	de tratamiento de un paciente.
	Para esto un tecnico accede al sistema y vizualiza la informacion.
Antecedentes: 
	Dado que el tecnico "Jose" "Labayru" se encuentra registrada en el sistema
	Y que su username es "joselabayru" y su password es "imthenight"
	Y el paciente "Fernando" "Castillo" con rut 18175963 esta ingresado y hospitalizado
	Y tiene asignado Paracetamol de 500mg a las 8am y 4 pm todos los dias  

Escenario: Se vizualiza el tratamiento de un paciente
	Dado que el tecnico "Jose" "Labayru" ha ingresado al sistema
	Y se encuentra en la vista para vizualizar tratamientos
	Cuando el tecnico seleccione a "Fernando" "Castillo" de la lista de pacientes
	Entonces vera que el paciente tiene asignado Paracetamol de 500mg 
	Y que debe administrase 2 veces al dia a las 8am y 4pm todos los dias