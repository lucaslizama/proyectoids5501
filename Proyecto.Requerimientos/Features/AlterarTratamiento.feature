﻿Característica: Alterar el tratamiento de un paciente
	En ocaciones sera necesario modificar el tratamiento de un paciente
	Aumentar o disminuir la dosis de algun medicamento, o cambiar los horarios
	de administracion.
	Para hacer esto un tecnico debe acceder al sistema y realizar el cambio.

Antecedentes: 
	Dado que el tecnico "Gabriel" "Torres" se encuentra registrado en el sistema
	Y que su username es "gtorres" y su password es "password123"
	Y el paciente "Jorge" "Lizama" con rut 18464695 esta hospitalizado

Escenario: Se altera un tratamiento
	Dado que el tecnico "Gabriel" "Torres" ha ingresado al sistema
	Y se encuentra en la vista para modificar tratamientos
	Cuando el tecnico seleccione a "Jorge" "Lizama" de la lista de pacientes
	Entonces vera cada uno de los medicamentos asignados, sus cantidades y horarios
	Y podra editarlos o modificarlos 
	Cuando el tecnico modifique un tratamiento y precione guardar
	Entonces el tratamiento sera guardado
	Y el cambio registrado al tecnico "Gabriel" "Torres"