﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.0.0.0
//      SpecFlow Generator Version:2.0.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Proyecto.Requerimientos.Features
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.0.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("Administrar el tratamiento a un paciente")]
    public partial class AdministrarElTratamientoAUnPacienteFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "AdministrarTratamiento.feature"
#line hidden
        
        [NUnit.Framework.TestFixtureSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("es"), "Administrar el tratamiento a un paciente", "\tPara poder administrar un tratamiento a un paciente\r\n\tun tecnico debe acceder al" +
                    " sistema y dejar constancia\r\n\tde que el tratamiento fue administrado", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.TestFixtureTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 6
#line 7
 testRunner.Given("que el tecnico \"Gabriel\" \"Torres\" se encuentra registrado en el sistema.", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Dado ");
#line 8
 testRunner.And("que su username es \"gtorres\" y su password es \"password123\".", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Y ");
#line 9
 testRunner.And("el paciente \"Jorge\" \"Lizama\" con rut 18464695 tambien ingresado necesita su trata" +
                    "miento.", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Y ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("El tecnico administra un tratamiento exitosamente")]
        public virtual void ElTecnicoAdministraUnTratamientoExitosamente()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("El tecnico administra un tratamiento exitosamente", ((string[])(null)));
#line 11
this.ScenarioSetup(scenarioInfo);
#line 6
this.FeatureBackground();
#line 12
 testRunner.Given("que el tecnico Gabriel Torres ha ingresado al sistema", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Dado ");
#line 13
 testRunner.And("se encuentra en la vista para administrar tratamientos", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Y ");
#line 14
 testRunner.When("el tecnico seleccione a Jorge Lizama de la lista de pacientes", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Cuando ");
#line 15
 testRunner.Then("vera los tratamientos y sus horarios", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Entonces ");
#line 16
 testRunner.And("podra marcarlos como \"administrado\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Y ");
#line 17
 testRunner.When("el tecnico marque un tratamiento como \"administrado\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Cuando ");
#line 18
 testRunner.Then("quedara constancia de que Gabriel Torres administro el tratamiento.", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Entonces ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
