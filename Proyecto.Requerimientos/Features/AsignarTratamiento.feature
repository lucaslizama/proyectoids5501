﻿Característica: Asignar tratamiento a un paciente
	Una vez que un paciente ha sido hospitalizado es necesario
	ingresar al sistema los medicamentos, sus dosis y los horarios
	de administracion asignados por el medico al sistema.
	Para hacer esto un tecnico accede al sistema e ingresa el tratamiento.

Antecedentes: 
	Dado que la tecnico "Francisa" "De Negri" se encuentra registrada en el sistema
	Y que su username es "dene092" y su password es "8330719"
	Y el paciente "Ian" "Genkowsky" con rut 18684259 esta recien hospitalizado

Escenario: Se asigna un tratamiento a un paciente
	Dado que la tecnico "Francisca" "De Negri" ha ingresado al sistema
	Y se encuentra en la vista para asignar tratamientos
	Cuando la tecnico seleccione a "Ian" "Genkowsky" de la lista de pacientes
	Entonces vera una lista vacia de tratamientos
	Y podra agregar uno o mas nuevos tratamientos
	Cuando el tecnico precione el boton agregar
	Y Seleccione "Clonazepam" como medicamento
	Y 0.5 mg como dosis
	Y Como horario Lunes ,Miercoles y Viernes a las 4:00PM
	Cuando precione el boton guardar 
	Entonces el tratamiento sera asignado al paciente
	Y el cambio registrado a la tecnico "Francisca" "De Negri"
