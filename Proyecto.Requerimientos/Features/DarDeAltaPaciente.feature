﻿Característica: Dar de alta a un paciente
	Una vez que un paciente es dado de alta en el hospital
	es necesario marcarlo en el sistema como "Dado de Alta",
	sin borrar el registro del paciente en caso de que
	volviera a necesitar hospitalizacion.
	Para esto un tecnico ingresa al sistema y cambia el 
	estado del paciente.

Antecedentes: 
	Dado que la tecnico "Francisa" "De Negri" se encuentra registrada en el sistema
	Y que su username es "dene092" y su password es "8330719"
	Y el paciente "Ian" "Genkowsky" con rut 18684259 esta recien hospitalizado

Escenario: Se da de alta a un Paciente
	Dado que la tecnico "Francisca" "De Negri" ha ingresado al sistema
	Y se encuentra en la vista para cambiar el estado de un paciente
	Cuando la tecnico seleccione a "Ian" "Genkowsky" de la lista de pacientes
	Entonces vera, entre otros datos, el estado del paciente como "Hospitalizado"
	Y podra cambiar este estado
	Cuando la tecnico cambie el estado a "Dado de Alta"
	Y precion el boton guardar
	Entonces el paciente sera marcado como "Dado de Alta"
	Y este cambio quedara registrado a la tecnico "Francisca" "De Negri"
