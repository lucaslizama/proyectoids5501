﻿Característica: Ingresar un paciente al sistema
	Cuando un paciente sea hospitalizado y su doctor le asigne
	sus medicamentos, sera necesario ingresarlo por primera vez
	si es que no ha sido hospitalizado previamente.
	Para esto un tecnico accede al sistema he ingresa los datos
	del paciente.
Antecedentes:
	Dado que el tecnico "Jose" "Labayru" se encuentra registrada en el sistema
	Y que su username es "joselabayru" y su password es "imthenight"
	Y el paciente "Fernando" "Castillo" con rut 18175963 aun no se encuentra ingresado
Escenario: Se ingresa por primera vez a un paciente
	Dado que el tecnico "Jose" "Labayru" ha ingresado al sistema
	Y se encuentra en el formulario de ingreso de pacientes
	Cuando el tecnico ingrese el rut,nombre y apellido del paciente
	Y precione el boton ingresar
	Entonces el paciente sera ingresado al sistema
	Y esta operacion registrada al tecnico "Jose" "Labayru"
