﻿Característica: Login Tecnico
	Para poder realizar acciones en el sistema
	un tecnico primero debe validarse 
	haciendo login con sus datos.
	
Antecedentes: 
	Dado que se encuentran registrados los siguientes tecnicos en el sistema
	| nombre  | apellido | username    | password    |
	| Lucas   | Lizama   | lucaslizama | triforce#   |
	| Gabriel | Torres   | gtorres     | password123 |

Esquema del escenario: Tecnico hace login satisfactoriamente
	Dado que el tecnico <nombre> <apellido> abre la aplicacion
	Y se encuentra en la vista de login
	Y ingresa su <username> y <password> 
	Cuando precione el boton de login
	Entonces se vera loggeado como <username> en la app
Ejemplos: 
	| nombre  | apellido | username    | password    |
	| Lucas   | Lizama   | lucaslizama | triforce#   |
	| Gabriel | Torres   | gtorres     | password123 |