﻿Característica: Administrar el tratamiento a un paciente
	Para poder administrar un tratamiento a un paciente
	un tecnico debe acceder al sistema y dejar constancia
	de que el tratamiento fue administrado

Antecedentes: 
	Dado que el tecnico "Gabriel" "Torres" se encuentra registrado en el sistema.
	Y que su username es "gtorres" y su password es "password123".
	Y el paciente "Jorge" "Lizama" con rut 18464695 tambien ingresado necesita su tratamiento.

Escenario: El tecnico administra un tratamiento exitosamente
	Dado que el tecnico Gabriel Torres ha ingresado al sistema
	Y se encuentra en la vista para administrar tratamientos
	Cuando el tecnico seleccione a Jorge Lizama de la lista de pacientes
	Entonces vera los tratamientos y sus horarios
	Y podra marcarlos como "administrado"
	Cuando el tecnico marque un tratamiento como "administrado"
	Entonces quedara constancia de que Gabriel Torres administro el tratamiento.
